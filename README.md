# README
Pedro L. Luque  
5 de agosto de 2016  



# Introducción a GitLab


# Práctica real de creación de un proyecto

1. En primer lugar, cree una cuenta gratuita en <http://gitlab.com>

La dirección de mis repositorios-proyectos principal es:

<https://gitlab.com/calote/>


1. Creo la key para colocar en GitLab, para ello abro el terminal en mi macbook pro en el raíz del usuario, y ejecuto las siguientes instrucciones para crear una ssh-key:

```
cd ~/.ssh
ls -la
ssh-keygen -t rsa -C "pedro.luis.luque@gmail.com"
ls -la 
cat id_rsa.pub
```


Copiar la salida del terminal correspondiente a la orden `cat id_rsa.pub` en GitLab.com al seleccionar: 

- (esquina derecha superior) Profile Settings
- (barra menú superior) SSH Keys
- (copiar la salida en) Key
- (poner un pequeño comentario de que ordenador es, en) Title
- (pulsar) Add key


Consultar la ayuda **generate it.** en: <https://gitlab.com/help/ssh/README>


2. Cree mi primer proyecto (repositorio), que llamé: **`Mi_primer_proyecto_GitLab**. 

Las carácterísticas del proyecto fueron:
  - público

La dirección del repositorio es:

<https://gitlab.com/calote/Mi_primer_proyecto_GitLab>


Al crearlo aparecen las siguientes instrucciones para crear mi repositorio en mi ordenador local:


![](graficos/gitlab01.png)

![](graficos/gitlab02.png)

![](graficos/gitlab03.png)


**Command line instructions**


Git global setup

```
git config --global user.name "Pedro"
git config --global user.email "pedro.luis.luque@gmail.com"
```

Create a new repository

```
git clone git@gitlab.com:calote/Mi_primer_proyecto_R.git
cd Mi_primer_proyecto_R
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

Existing folder or Git repository

```
cd existing_folder
git init
git remote add origin git@gitlab.com:calote/Mi_primer_proyecto_R.git
git add .
git commit
git push -u origin master
```

4. Abro el terminal en el mac, en el directorio del usuario y ejecuto las siguientes instrucciones:

```
git clone git@gitlab.com:calote/Mi_primer_proyecto_R.git
cd Mi_primer_proyecto_R
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

```

Y se ha creado el directorio "Mi_primer_proyecto_R", desde la carpeta de usuario.

5. Creo un proyecto nuevo desde RStudio a partir de un directorio existente, eligiendo el directorio que contiene el proyecto-repositorio creado en GitLab. Al abrirse, reconocerá que tiene git activado.

Podré hacer las operaciones sobre el Panel "Git" habituales para mantener el proyecto:

  - Commit (añadiendo nuevos comentarios)
  - Push (para subir el nuevo material, que he commitado)

6. Se puede mover de sitio la carpeta a otro destino (hecho en mi mismo macbook pro), y todo funciona correctamente: commit->push.


## Enlaces de ayuda sobre GitLab

### Uso con SourceTree




### Uso con RStudio
